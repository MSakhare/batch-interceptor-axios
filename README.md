## Prerequisites to run project
### Step 1: Installations 
 - Node (>=10)
 - yarn (1.22.4)

### Step 2: Installing project dependencies
Run the command `yarn install`

### Step 3: Building the project 
Run the command  `yarn build`

### Step 4: Run the server 
Run the command: `yarn serve`

**YOHO! You may now access the web app at**

-- http://localhost:8080
