const path = require('path');

module.exports = {
    entry: './src/test.js',
    mode: "development",
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist'),
    },
};
