import * as axios from "../node_modules/axios";
import batchInterceptor from "./batchInterceptor.js";
const client = () => {
    const config = {
        host: "https://europe-west1-quickstart-1573558070219.cloudfunctions.net",
        baseURL: "https://europe-west1-quickstart-1573558070219.cloudfunctions.net",
        headers: {}
    };

    const instance = axios.create(config);
    batchInterceptor(instance);
    return instance;
}
export default client;
