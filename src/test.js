import client from "./apiClient";
// All requests should run at the same time and produce only one request to the backend. All requests should return or reject.

const api = client();
// export const test = function () {
    function runTest() {
        let articleElements = document.getElementsByTagName('textarea');
        const batchUrl = "/file-batch-api";
// Should return [{id:”fileid1”},{id:”fileid2”}]
        api.get(batchUrl, {params: {ids: ["fileid1","fileid2"]}}).then(res => {
            console.log("First API:");
            console.log(res);
            articleElements[0].innerHTML = JSON.stringify(res.data);
            articleElements[0].setAttribute('style', "display: block;");
        }).catch(err => {
            console.log(err);
        });
// Should return [{id:”fileid2”}]
    api.get(batchUrl, {params: {ids: ["fileid2"]}}).then(res => {
        console.log("Second API:");
        console.log(res);
        articleElements[1].innerHTML = JSON.stringify(res.data);
        articleElements[1].setAttribute('style', "display: block;");
    }).catch(err => {
        console.log(err);
    });
// Should reject as the fileid3 is missing from the response
    api.get(batchUrl, {params: {ids: ["fileid3"]}}).then(res => {
        console.log("Third API:");
        console.log(res);
        articleElements[2].innerHTML = JSON.stringify(res.data);
        articleElements[2].setAttribute('style', "display: block;");
    }).catch(err => {
        console.log(err);
        articleElements[2].innerHTML = JSON.stringify(err.message);
        articleElements[2].setAttribute('style', "display: block;");
    });

    }

    window.onload = (()=> {
        document.getElementsByTagName("button")[0].addEventListener('click',() => {
           runTest();
        });
    })
/*
    runTest();*/
// }
